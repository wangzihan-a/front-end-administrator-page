import { createRouter, createWebHistory } from 'vue-router'

//引入布局组件
import Layout from '../views/Layout/index.vue'
const routes = [
  {
    path: '/',
    name: 'Login',
    hidden: true,
    meta: {
      name: '登录'
    },
    //按需加载
    component: () => import('../views/Login.vue')
  },
  {
    path: '/:catchAll(.*)',
    name: '404',
    hidden: true,
    meta: {
      name: '404页面'
    },
    //按需加载
    component: () => import('../views/404/404.vue')
  },

  {
    path: '/carousel',
    name: 'system',
    //重定向
    meta: {
      name: '公告管理',
      icon: 'Stopwatch'
    },
    //控制台
    component: Layout,
    children: [
      {
        path: '/notice',
        name: 'Notice',
        meta: {
          name: '医院公告',
        },
        component: () => import('../views/System/notice.vue')
      },
      {
        path: '/carousel',
        name: 'carousel',
        meta: {
          name: '轮播图管理',
        },
        component: () => import('../views/Carousel/index.vue')
      },
      {
        path: '/news',
        name: 'news',
        meta: {
          name: '新闻界面',
        },
        component: () => import('../views/chenxing/news.vue')
      },
      {
        path: '/user',
        name: 'user',
        meta: {
          name: '个人信息',
        },
        component: () => import('../views/chenxing/user.vue')
      },
    ]
  },
  {
    path: '/Room',
    name: 'Room',
    redirect: 'index',
    //从定向
    meta: {
      name: '科室管理',
      icon: 'grid'
    },
    //控制台
    component: Layout,
    children: [
      {
        path: '/RoomChild',
        name: 'RoomChild',
        meta: {
          name: '科室管理',
        },
        component: () => import('../views/wangzihan/Room.vue')
      }
    ]
  },
  {
    path: '/carousel',
    name: 'dep',
    //重定向
    meta: {
      name: '用户角色管理',
      icon: 'Stopwatch'
    },
    //控制台
    component: Layout,
    children: [
      {
        path: '/UserRole',
        name: 'userrole',
        meta: {
          name: '用户角色管理',
        },
        component: () => import('../views/wangzihan/UserRole.vue')
      },
    
    ]
  },
  {
    path: '/Doctor',
    name: 'Doctor',
    redirect: 'index',
    //从定向
    meta: {
      name: '医生管理',
      icon: 'opportunity'
    },
    //控制台
    component: Layout,
    children: [
      {
        path: '/DoctorChild',
        name: 'DoctorChild',
        meta: {
          name: '医生管理',
        },
        component: () => import('../views/wangzihan/Doctor.vue')
      },
      {
        path: '/Schedulingchild',
        name: 'Schedulingchild',
        meta: {
          name: '排班管理',
        },
        component: () => import('../views/wangzihan/Scheduling.vue')
      },
   
    ]
  },
  {
    path: '/Patient',
    name: 'Patient',
    redirect: 'index',
    //从定向
    meta: {
      name: '患者',
      icon: 'link'
    },
    //控制台
    component: Layout,
    children: [
      {
        path: '/PatientChild',
        name: 'PatientChild',
        meta: {
          name: '患者管理',
        },
        component: () => import('../views/wangzihan/Patient.vue')
      }
    ]
  },

]

const router = createRouter({
  history: createWebHistory(process.env.BASE_URL),
  routes
})


export default router
